package com.lorenacadena.rest.repositorios;

import com.lorenacadena.rest.empleados.Capacitacion;
import com.lorenacadena.rest.empleados.Empleado;
import com.lorenacadena.rest.empleados.Empleados;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Repository
public class EmpleadoDAO {
    Logger logger = LoggerFactory.getLogger(EmpleadoDAO.class);

    private static Empleados list = new Empleados();
    static {
        Capacitacion cap1 = new Capacitacion("2019/06/15", "ASO");
        Capacitacion cap2 = new Capacitacion("2020/10/01", "DBA");
        Capacitacion cap3 = new Capacitacion("2021/04/30", "BACKEND");

        ArrayList<Capacitacion> una = new ArrayList<>();
        una.add(cap1);

        ArrayList<Capacitacion> dos = new ArrayList<>();
        dos.add(cap1);
        dos.add(cap2);

        ArrayList<Capacitacion> tres = new ArrayList<>();
        tres.addAll(dos);
        tres.add(cap3);

        list.getListEmpleados().add(new Empleado(1, "César Osvaldo",
                "Sánchez", "cesarosvaldo.sanchez@bbva.com", una));
        list.getListEmpleados().add(new Empleado(2, "Lorena",
                "Cadena", "lorena.cadena@bbva.com", tres));
        list.getListEmpleados().add(new Empleado(3, "German",
                "Vargas", "german.vargas@bbva.com", dos));
    }

    public Empleados getAllEmpleados() {
        return list;
    }

    public Empleado getEmpleado(int id){
        for( Empleado emp: list.getListEmpleados()){
            if( emp.getId() == id) {
                return emp;
            }
        }

        return null;
    }

    public void addEmpleado(Empleado emp) {
        list.getListEmpleados().add(emp);
    }

    public void updEmpleado(Empleado emp){
        Empleado current = getEmpleado(emp.getId());
        //validar el caso de emp = NULL
        current.setNombre(emp.getNombre());
        current.setApellido(emp.getApellido());
        current.setEmail(emp.getEmail());
    }

    public void updEmpleado(int id, Empleado emp){
        Empleado current = getEmpleado(id);
        //validar el caso de emp = NULL
        current.setNombre(emp.getNombre());
        current.setApellido(emp.getApellido());
        current.setEmail(emp.getEmail());
    }

    public String deleteEmpleado(int id) {
        Empleado current = getEmpleado( id );
        if( current == null)
            return "NF";

        Iterator it = list.getListEmpleados().iterator();
        while ( it.hasNext() ){
            Empleado emp = (Empleado) it.next();
            if( emp.getId() == id ){
                it.remove();
                break;
            }
        }
        return "OK";
    }

    public void softUpdEmpleado(int id, Map<String, Object> updates){
        Empleado current = getEmpleado(id);
        for( Map.Entry<String, Object> update : updates.entrySet()){
            switch ( update.getKey() ){
                case "nombre":
                    current.setNombre( update.getValue().toString());
                    break;
                case "apellido":
                    current.setApellido( update.getValue().toString() );
                    break;
                case "email":
                    current.setEmail( update.getValue().toString() );
                    break;
            }
        }
    }

    public List<Capacitacion> getCapsEmpleado( int id){
        Empleado current = getEmpleado(id);

        ArrayList<Capacitacion> caps = new ArrayList<>();
        if( current != null )
            caps = current.getCapacitaciones();

        return caps;
    }

    public boolean addCapacitacion(int id, Capacitacion cap){
        Empleado current = getEmpleado( id );
        if( current == null ) return false;

        current.getCapacitaciones().add(cap);
        return true;
    }
}