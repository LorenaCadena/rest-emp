package com.lorenacadena.rest.utils;

public class Utilidades {
    public static String getCadena(String texto, String separador) {
        char[] letras = texto.toUpperCase().toCharArray();
        String resultado = "";
        for (char letra : letras) {
            if (letra != ' ') {
                resultado += letra + separador;
            } else {
                if (!resultado.trim().equals("")) {
                    resultado = resultado.substring(0, resultado.length() -1);
                }
                resultado += letra;
            }
        }

        if (resultado.endsWith(separador)) {
            resultado = resultado.substring(0, resultado.length() -1);
        }
        return resultado;
    }
}
