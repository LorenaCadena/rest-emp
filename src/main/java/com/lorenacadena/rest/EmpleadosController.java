package com.lorenacadena.rest;

import com.lorenacadena.rest.empleados.Capacitacion;
import com.lorenacadena.rest.empleados.Empleado;
import com.lorenacadena.rest.empleados.Empleados;
import com.lorenacadena.rest.repositorios.EmpleadoDAO;
import com.lorenacadena.rest.utils.Configuracion;
import com.lorenacadena.rest.utils.Utilidades;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping(path = "/empleados")
public class EmpleadosController {
    Logger logger = LoggerFactory.getLogger(EmpleadosController.class);

    @Autowired
    private EmpleadoDAO empDao;

    @GetMapping(path = "/")
    public Empleados getEmpleados(){
        logger.debug("Empleados devueltos");
        return empDao.getAllEmpleados();
    }

    @GetMapping(path = "/{id}")
    //public Empleado getEmpleado(@PathVariable int id){
    public ResponseEntity<Empleado> getEmpleado(@PathVariable int id){
        //return empDao.getEmpleado( id );
        Empleado emp = empDao.getEmpleado( id );
        if( emp == null){
            return ResponseEntity.notFound().build();
        }else{
            return ResponseEntity.ok().body( emp );
        }
    }

    @PostMapping(path = "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> addEmpleado(@RequestBody Empleado emp){
        Integer id = empDao.getAllEmpleados().getListEmpleados().size() + 1;
        emp.setId(id);

        empDao.addEmpleado(emp);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(emp.getId())
                .toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping(path = "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updEmpleado(@RequestBody Empleado emp){
        empDao.updEmpleado( emp );

        return ResponseEntity.ok().build();
    }

    @PutMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updEmpleado(
            @PathVariable int id, @RequestBody Empleado emp){
        empDao.updEmpleado( id, emp );

        return ResponseEntity.ok().build();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Object> delEmpleadoById(@PathVariable int id){
        String result = empDao.deleteEmpleado(id);
        if(result == "OK"){
            return ResponseEntity.ok().build();
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    @PatchMapping(path = "/{id}", consumes = "application/json")
    public ResponseEntity<Object> softUpdEmpleado( @PathVariable int id,
                                                   @RequestBody Map<String, Object> updates){
        empDao.softUpdEmpleado( id, updates);
        return ResponseEntity.ok().build();

    }

    @GetMapping(path = "/{id}/capacitaciones")
    public ResponseEntity<List<Capacitacion>> getCapsEmpleado( @PathVariable int id){
        return ResponseEntity.ok().body(empDao.getCapsEmpleado( id ));
    }

    @PostMapping(path = "/{id}/capacitaciones", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> addCapacitaciones(@PathVariable int id, @RequestBody Capacitacion cap){
       if (empDao.addCapacitacion( id, cap)) {
           return ResponseEntity.ok().build();
       }else{
           return ResponseEntity.notFound().build();
       }
    }

    @Value("${app.titulo}") private String titulo;

    @GetMapping("/titulo")
    public String getAppTitulo(){
        //return titulo;
        String modo = configuracion.getModo();

        return String.format("%s (%s)", titulo, modo);
    }

    @Autowired
    private Environment env;

    @Autowired
    Configuracion configuracion;

    @GetMapping("/autor")
    public String getAppAutor(){
        //Configuracion configuracion = new Configuracion();
        return configuracion.getAutor();
        //return env.getProperty("app.autor");
    }

    @GetMapping("/cadena")
    public String getCadena(@RequestParam String texto, @RequestParam String separador){
        return Utilidades.getCadena(texto, separador);
    }

}


